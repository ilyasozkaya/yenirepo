﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using MyApi.Model;
using MyApi.Repository;

namespace MyApi.Controllers
{
  [Route("api/[controller]")]
  public class AuthController : Controller
  {
    private UserManager<ApplicationUser> userManager;
    private IRepository<ApplicationUser> _repo;
    private IRepository<Log> _repoLog;
    private readonly IHttpContextAccessor httpContextAccessor;

    private readonly IUnitOfWork _unitOfWork;
    public AuthController(UserManager<ApplicationUser> userManager, IRepository<Log> repoLog, IRepository<ApplicationUser> repo, IHttpContextAccessor httpContextAccessor)
    {
      this.userManager = userManager;
      _repoLog = repoLog;
      _repo = repo;
      this.httpContextAccessor = httpContextAccessor;
    }

    [HttpPost]
    [Route("Register")]
    public async Task<IActionResult> Register([FromBody] RegisterModel model)
    {
      try
      {
        ApplicationUser newUser = new ApplicationUser(); //Yeni kullanıcı oluşturma.
        newUser.Email = model.email;
        newUser.PhoneNumber = model.phoneNumber;
        newUser.UserName = model.userName;
        //newUser.PasswordHash = userManager.PasswordHasher.HashPassword(newUser, model.password);
        // _repo.Add(newUser);
        var resut = await userManager.CreateAsync(newUser, model.password);

        //log
        Log l = new Log();
        //get ip address
        string ip = this.httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();
        l.ipAddress = ip;
        l.Date = System.DateTime.Now;
        l.Description = "AuthController Register için başarılı işlem";
        _repoLog.Add(l);
        return Json(true);
      }
      catch (Exception ex)
      {
        //log
        Log l = new Log();
        //get ip address
        string ip = this.httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();
        l.ipAddress = ip;
        l.Date = System.DateTime.Now;
        l.Description = "AuthController Register için hatalı işlem";
        l.Exception = ex.Message;
        _repoLog.Add(l);
        return Json(false);
      }
    }

    [Route("Login")]
    [HttpPost]
    public async Task<IActionResult> Login([FromBody]LoginModel model)
    {
      //log
      Log l = new Log();
      //get ip address
      string ip = this.httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();
      l.ipAddress = ip;
      l.Date = System.DateTime.Now;
      l.Description = "AuthController Login denemesi işlem";
      _repoLog.Add(l);
      try
      {
        // _repo kullanılarakda yapılabilirdi.
        var user = await userManager.FindByNameAsync(model.UserName);
        if (user != null && await userManager.CheckPasswordAsync(user, model.Password))
        {

          var claims = new[]
          {
                    new Claim (JwtRegisteredClaimNames.Sub,user.UserName),
                    new Claim(JwtRegisteredClaimNames.Jti,Guid.NewGuid().ToString())
                };
          var signingKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("MySuperSecureKey"));

          var token = new JwtSecurityToken(
              issuer: "http://oec.com",
              audience: "http://oec.com",
              expires: DateTime.Now.AddHours(1),
              claims: claims,
              signingCredentials: new Microsoft.IdentityModel.Tokens.SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256)
              );
          return Ok(new
          {
            token = new JwtSecurityTokenHandler().WriteToken(token),
            expiration = token.ValidTo
          }); //Kullanıcı için bir token oluşturuluyor. Api ye bu token ile gelmeli ve expire oldugunda tekrar token almalı.
        }
        return Unauthorized();
      }
      catch (Exception ex)
      {
        //log
        Log l2 = new Log();
        //get ip address
        string ip2 = this.httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();
        l2.ipAddress = ip2;
        l2.Date = System.DateTime.Now;
        l2.Description = "AuthController Login için hatalı işlem";
        l2.Exception = ex.Message;
        _repoLog.Add(l2);
        return Json(false);
      }
    }

    [Route("ResetPassword")]
    [Authorize]
    [HttpPost]
    public async Task<IActionResult> ResetPassword(string email, string newPassword)
    {
      ApplicationUser user = await userManager.FindByEmailAsync(email);
      if (user != null)
      {
        try
        {

          user.PasswordHash = userManager.PasswordHasher.HashPassword(user, newPassword);
          _repo.Update(user);
          //log
          Log l = new Log();
          //get ip address
          string ip = this.httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();
          l.ipAddress = ip;
          l.Date = System.DateTime.Now;
          l.Description = "AuthController ResetPassword için başarılı işlem";
          _repoLog.Add(l);
          return Json(true);
        }
        catch (Exception ex)
        {
          //log
          Log l = new Log();
          //get ip address
          string ip = this.httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();
          l.ipAddress = ip;
          l.Date = System.DateTime.Now;
          l.Description = "AuthController ResetPassword için hatalı işlem";
          l.Exception = ex.Message;
          _repoLog.Add(l);
          return Json(false);
        }
      }
      else
      {
        return Json(false);
      }
    }


    [HttpGet("test")]
    public string test()
    {
      City c = new City();
      c.Name = "ilyas";
      _unitOfWork.GetRepository<City>().AddAsync(c);
      return "ok";
    }
  }
}
﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MyApi.Model;
using MyApi.Repository;

namespace MyApi.Controllers
{
  [Route("api/[controller]")]
  [Authorize]
  public class UserController : Controller
  {
    private IRepository<ApplicationUser> _repo;
    private IRepository<Log> _repoLog;
    private IRepository<Adverts> _repoAdvert;
    private IRepository<Bids> _repoBid;
    private IRepository<Messages> _repoMessage;
    private readonly IHttpContextAccessor httpContextAccessor;
    private readonly IRepository<Log> _RepoL;
    private readonly IRepository<Images> _Repoi;
    private IConfiguration _config;
    public UserController(IRepository<ApplicationUser> repo, IRepository<Log> repoLog, IRepository<Adverts> repoAdvert, IRepository<Bids> repoBid, IRepository<Messages> repoMessage, IHttpContextAccessor httpContextAccessor, IRepository<Log> RepoL, IConfiguration config, IRepository<Images> Repoi)
    {
      _repo = repo;
      _repoLog = repoLog;
      _repoAdvert = repoAdvert;
      _repoBid = repoBid;
      _repoMessage = repoMessage;
      _RepoL = RepoL;
      _Repoi = Repoi;
      this.httpContextAccessor = httpContextAccessor;
      _config = config;
    }


    [Route("AddAdvert")]
    [HttpPost]
    public IActionResult AddAdvert([FromBody] Adverts model)
    {
      try
      {
        _repoAdvert.Add(model);
        //log
        Log l = new Log();
        //get ip address
        string ip = this.httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();
        l.ipAddress = ip;
        l.Date = System.DateTime.Now;
        l.Description = "UserController AddAdvert için başarılı işlem";
        _RepoL.Add(l);
        return Json(true);
      }
      catch (Exception ex)
      {
        //log
        Log l = new Log();
        //get ip address
        string ip = this.httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();
        l.ipAddress = ip;
        l.Date = System.DateTime.Now;
        l.Description = "UserController AddAdvert için hatalı işlem";
        l.Exception = ex.Message;
        _RepoL.Add(l);
        return Json(false);
      }
    }

    [Route("AddImage")]
    [HttpPost]
    public IActionResult AddImage([FromBody]ProductImageModel model)
    {
      if(model!=null&&!string.IsNullOrEmpty(model.Image1))
      {
        Images image = new Images();
        string filePath = _config.GetSection("MySettings").GetSection("Imagepath").Value; // C:\Test\
        if (!Directory.Exists(filePath))
          Directory.CreateDirectory(filePath); // Kayıt klasörü mevcut değilse oluştur.
        string imageName = Guid.NewGuid().ToString();
        filePath = filePath + imageName + ".jpeg";

        var bytes = Convert.FromBase64String(model.Image1);
        using (var imageFile = new FileStream(filePath, FileMode.Create))
        {          
          imageFile.Write(bytes, 0, bytes.Length);
          imageFile.Flush();
          image.Image1 = filePath;
          filePath = _config.GetSection("MySettings").GetSection("Imagepath").Value;
        }
        if(!string.IsNullOrEmpty(model.Image2))
        {
           imageName = Guid.NewGuid().ToString();
          filePath = filePath + imageName + ".jpeg";

           bytes = Convert.FromBase64String(model.Image2);
          using (var imageFile = new FileStream(filePath, FileMode.Create))
          {
            imageFile.Write(bytes, 0, bytes.Length);
            imageFile.Flush();
            image.Image2 = filePath;
            filePath = _config.GetSection("MySettings").GetSection("Imagepath").Value;
          }
        }
        if (!string.IsNullOrEmpty(model.Image3))
        {
          imageName = Guid.NewGuid().ToString();
          filePath = filePath + imageName + ".jpeg";

          bytes = Convert.FromBase64String(model.Image3);
          using (var imageFile = new FileStream(filePath, FileMode.Create))
          {
            imageFile.Write(bytes, 0, bytes.Length);
            imageFile.Flush();
            image.Image3 = filePath;
          }
        }
        _Repoi.Add(image);
        int imageId = _Repoi.OrderBy(x => x.Id, true).First().Id;

        return Json(imageId);
      }
      //
      return Json(false);
    }

    [Route("UpdateAdvert")]
    [HttpPost]
    public IActionResult UpdateImageAdvert(int imageId,Images model)
    {
      return Json(true);
    }

    [Route("GetMyAdverts")]
    [HttpGet]
    public IActionResult GetMyAdverts(string userId)
    {
      try
      {
        //log
        Log l = new Log();
        //get ip address
        string ip = this.httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();
        l.ipAddress = ip;
        l.Date = System.DateTime.Now;
        l.Description = "UserController GetMyAdvert istegi";
        _RepoL.Add(l);
        return Json(_repoAdvert.Where(x => x.UserId == userId).ToList());
      }
      catch (Exception ex)
      {
        //log
        Log l = new Log();
        //get ip address
        string ip = this.httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();
        l.ipAddress = ip;
        l.Date = System.DateTime.Now;
        l.Description = "UserController GetMyAdvert için hatalı işlem";
        l.Exception = ex.Message;
        _RepoL.Add(l);
        return Json(false);
      }
      
    }

    [Route("SendMessage")]
    [HttpPost]
    public IActionResult SendMessage(string senderId, string receipentId, string message)
    {
      try
      {
        Messages m = new Messages();
        m.SenderUserId = senderId;
        m.ReceipentUserId = receipentId;
        m.MessageContent = message;
        m.SendDate = System.DateTime.Now;
        _repoMessage.Add(m);
        //log
        Log l = new Log();
        //get ip address
        string ip = this.httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();
        l.ipAddress = ip;
        l.Date = System.DateTime.Now;
        l.Description = "UserController SendMessage için başarılı işlem";
        _RepoL.Add(l);
        return Json(true);
      }
      catch (Exception ex)
      {
        //log
        Log l = new Log();
        //get ip address
        string ip = this.httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();
        l.ipAddress = ip;
        l.Date = System.DateTime.Now;
        l.Description = "UserController SendMessage için hatalı işlem";
        l.Exception = ex.Message;
        _RepoL.Add(l);
        return Json(false);
      }

    }

    [Route("GiveBid")]
    [HttpPost]
    public IActionResult GiveBid([FromBody]Bids model)
    {
      try
      {
        _repoBid.Add(model);
        //log
        Log l = new Log();
        //get ip address
        string ip = this.httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();
        l.ipAddress = ip;
        l.Date = System.DateTime.Now;
        l.Description = "UserController GiveBid için başarılı işlem";
        _RepoL.Add(l);
        return Json(true);
      }
      catch (Exception ex)
      {
        //log
        Log l = new Log();
        //get ip address
        string ip = this.httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();
        l.ipAddress = ip;
        l.Date = System.DateTime.Now;
        l.Description = "UserController GiveBid için hatalı işlem";
        l.Exception = ex.Message;
        _RepoL.Add(l);
        return Json(false);
      }
    }

    [Route("AdvertUpdate")]
    [HttpPost]
    public IActionResult AdvertUpdate(int advertId,[FromBody]Adverts newAdvert)
    {
      try
      {
        Adverts old = _repoAdvert.Where(x => x.Id == advertId).FirstOrDefault();
        old.IsConfirmed = newAdvert.IsConfirmed;
        old.IsDeleted = newAdvert.IsDeleted;
        old.IsSold = newAdvert.IsSold;
        old.ModelId = newAdvert.ModelId;
        old.SubCategoryId = newAdvert.SubCategoryId;
        old.Title = newAdvert.Title;
        old.TrademarkId = newAdvert.TrademarkId;
        old.View = newAdvert.View;
        old.Warranty = newAdvert.Warranty;
        _repoAdvert.Update(old);
        //log
        Log l = new Log();
        //get ip address
        string ip = this.httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();
        l.ipAddress = ip;
        l.Date = System.DateTime.Now;
        l.Description = "UserController AdvertUpdate için başarılı işlem";
        _RepoL.Add(l);
        return Json(true);
      }
      catch (Exception ex)
      {
        //log
        Log l = new Log();
        //get ip address
        string ip = this.httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();
        l.ipAddress = ip;
        l.Date = System.DateTime.Now;
        l.Description = "UserController AdvertUpdate için hatalı işlem";
        l.Exception = ex.Message;
        _RepoL.Add(l);
        return Json(false);
      }
    }
    [Route("AdvertDelete")]
    [HttpPost]
    public IActionResult AdvertDelete(int advertId)
    {
      try
      {
        Adverts old = _repoAdvert.Where(x => x.Id == advertId).FirstOrDefault();
        old.IsDeleted = true;       
        _repoAdvert.Update(old);
        //log
        Log l = new Log();
        //get ip address
        string ip = this.httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();
        l.ipAddress = ip;
        l.Date = System.DateTime.Now;
        l.Description = "UserController AdvertDelete için başarılı işlem";
        _RepoL.Add(l);
        return Json(true);
      }
      catch (Exception ex)
      {
        //log
        Log l = new Log();
        //get ip address
        string ip = this.httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();
        l.ipAddress = ip;
        l.Date = System.DateTime.Now;
        l.Description = "UserController AdvertDelete için hatalı işlem";
        l.Exception = ex.Message;
        _RepoL.Add(l);
        return Json(false);
      }
    }
    [Route("FindAdvert")]
    [HttpPost]
    public IActionResult FindAdverts(int? categoryId,int? subCategoryId, int? trademarkId, int? modelId)
    {
      try
      {
        var advertList = _repoAdvert.All();
        if (categoryId != null)
          advertList = advertList.Where(x => x.CategoryId == categoryId);
        if(subCategoryId!=null)
          advertList = advertList.Where(x => x.SubCategoryId == subCategoryId);
        if(trademarkId!=null)
          advertList = advertList.Where(x => x.TrademarkId == trademarkId);
        if(modelId!=null)
          advertList = advertList.Where(x => x.ModelId == modelId);
        //log
        Log l = new Log();
        //get ip address
        string ip = this.httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();
        l.ipAddress = ip;
        l.Date = System.DateTime.Now;
        l.Description = "UserController FindAdverts için başarılı işlem";
        _RepoL.Add(l);
        return Json(advertList);
      }
      catch (Exception ex)
      {
        //log
        Log l = new Log();
        //get ip address
        string ip = this.httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();
        l.ipAddress = ip;
        l.Date = System.DateTime.Now;
        l.Description = "UserController FindAdverts için hatalı işlem";
        l.Exception = ex.Message;
        _RepoL.Add(l);
        return Json(false);
      }
    }
  }
}
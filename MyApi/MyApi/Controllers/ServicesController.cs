﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MyApi.Model;
using MyApi.Repository;

namespace MyApi.Controllers
{
    [Route("api/[controller]")]
    //[Authorize]
    public class ServicesController : Controller
    {
        private readonly IRepository<Category> _RepoC;
        private readonly IRepository<SubCategory> _RepoSC;
        private readonly IRepository<Trademark> _RepoT;
        private readonly IRepository<Models> _RepoM;
        private readonly IRepository<Adverts> _RepoA;
        private readonly IRepository<Color> _RepoCo;
        private readonly IRepository<Log> _RepoL;
        private readonly IHttpContextAccessor httpContextAccessor;

        public ServicesController(IRepository<Category> RepoC, IRepository<SubCategory> RepoSC, IRepository<Trademark> RepoT, IRepository<Models> RepoM, IRepository<Adverts> RepoA, IHttpContextAccessor httpContextAccessor, IRepository<Log> RepoL, IRepository<Color> RepoCo)
        {
            _RepoC = RepoC;
            _RepoSC = RepoSC;
            _RepoT = RepoT;
            _RepoM = RepoM;
            _RepoA = RepoA;
            _RepoL = RepoL;
            _RepoCo = RepoCo;
            this.httpContextAccessor = httpContextAccessor;
        }
        [HttpGet]
        [Route("getCategory")]
        public IActionResult getCategory()
        {
            try
            {
                //log
                Log l = new Log();
                //get ip address
                string ip = this.httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();
                l.ipAddress = ip;
                l.Date = System.DateTime.Now;
                l.Description = "ServicesController getCategory isteği";
                _RepoL.Add(l);
                return Json(_RepoC.Where(x => x.IsActive == true).ToList());

            }
            catch (Exception ex)
            {
                //log
                Log l = new Log();
                //get ip address
                string ip = this.httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();
                l.ipAddress = ip;
                l.Date = System.DateTime.Now;
                l.Description = "ServicesController getCategory için hatalı işlem";
                l.Exception = ex.Message;
                _RepoL.Add(l);
                return Json(false);
            }
        }
        [HttpGet]
        [Route("getSubCategory")]
        public IActionResult getSubCategory()
        {
            try
            {
                //log
                Log l = new Log();
                //get ip address
                string ip = this.httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();
                l.ipAddress = ip;
                l.Date = System.DateTime.Now;
                l.Description = "ServicesController getSubCategory isteği";
                _RepoL.Add(l);
                return Json(_RepoSC.Where(x => x.IsActive == true).ToList());
            }
            catch (Exception ex)
            {
                //log
                Log l = new Log();
                //get ip address
                string ip = this.httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();
                l.ipAddress = ip;
                l.Date = System.DateTime.Now;
                l.Description = "ServicesController getSubCategory için hatalı işlem";
                l.Exception = ex.Message;
                _RepoL.Add(l);
                return Json(false);
            }
        }
        [HttpGet]
        [Route("getTrademark")]
        public IActionResult getTrademark()
        {
            try
            {
                //log
                Log l = new Log();
                //get ip address
                string ip = this.httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();
                l.ipAddress = ip;
                l.Date = System.DateTime.Now;
                l.Description = "ServicesController getTrademark isteği";
                _RepoL.Add(l);
                return Json(_RepoT.Where(x => x.isActive == true).ToList());
            }
            catch (Exception ex)
            {
                //log
                Log l = new Log();
                //get ip address
                string ip = this.httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();
                l.ipAddress = ip;
                l.Date = System.DateTime.Now;
                l.Description = "ServicesController getTrademark için hatalı işlem";
                l.Exception = ex.Message;
                _RepoL.Add(l);
                return Json(false);
            }
        }
        [HttpGet]
        [Route("getModel")]
        public IActionResult getModel()
        {
            try
            {
                //log
                Log l = new Log();
                //get ip address
                string ip = this.httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();
                l.ipAddress = ip;
                l.Date = System.DateTime.Now;
                l.Description = "ServicesController getSubCategory isteği";
                _RepoL.Add(l);
                return Json(_RepoM.Where(x => x.isActive == true).ToList());
            }
            catch (Exception ex)
            {
                //log
                Log l = new Log();
                //get ip address
                string ip = this.httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();
                l.ipAddress = ip;
                l.Date = System.DateTime.Now;
                l.Description = "ServicesController getModel için hatalı işlem";
                l.Exception = ex.Message;
                _RepoL.Add(l);
                return Json(false);
            }

        }

        [HttpGet]
        [Route("LoadSubCategory/{id}")]
        public IActionResult LoadSubCategory(int id = 0)
        {
            try
            {
                //log
                Log l = new Log();
                //get ip address
                string ip = this.httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();
                l.ipAddress = ip;
                l.Date = System.DateTime.Now;
                l.Description = "ServicesController LoadSubCategory için başarılı işlem";
                _RepoL.Add(l);
                //return Json(_RepoSC.Where(x => x.CategoryId == Id).ToList());
                return Json(_RepoSC.Where(x => x.CategoryId == id).Select(x => new
                {
                    id = x.Id,
                    Name = x.Name
                }).ToList());
            }
            catch (Exception ex)
            {
                //log
                Log l = new Log();
                //get ip address
                string ip = this.httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();
                l.ipAddress = ip;
                l.Date = System.DateTime.Now;
                l.Description = "ServicesController LoadSubCategory için hatalı işlem";
                l.Exception = ex.Message;
                _RepoL.Add(l);
                return Json(false);
            }
        }

        [HttpGet]
        [Route("LoadTrademark/{id}")]
        public IActionResult LoadTrademark(int id = 0)
        {
            try
            {
                //log
                Log l = new Log();
                //get ip address
                string ip = this.httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();
                l.ipAddress = ip;
                l.Date = System.DateTime.Now;
                l.Description = "ServicesController LoadTrademark için başarılı işlem";
                _RepoL.Add(l);
                //return Json(_RepoSC.Where(x => x.CategoryId == Id).ToList());
                return Json(_RepoT.Where(x => x.SubCategoryId == id).Select(x => new
                {
                    id = x.Id,
                    Name = x.Name
                }).ToList());
            }
            catch (Exception ex)
            {
                //log
                Log l = new Log();
                //get ip address
                string ip = this.httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();
                l.ipAddress = ip;
                l.Date = System.DateTime.Now;
                l.Description = "ServicesController LoadTrademark için hatalı işlem";
                l.Exception = ex.Message;
                _RepoL.Add(l);
                return Json(false);
            }
        }

        [HttpGet]
        [Route("LoadModel/{id}")]
        public IActionResult LoadModel(int id = 0)
        {
            try
            {
                //log
                Log l = new Log();
                //get ip address
                string ip = this.httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();
                l.ipAddress = ip;
                l.Date = System.DateTime.Now;
                l.Description = "ServicesController LoadModel için başarılı işlem";
                _RepoL.Add(l);
                //return Json(_RepoSC.Where(x => x.CategoryId == Id).ToList());
                return Json(_RepoM.Where(x => x.TrademarkId == id).Select(x => new
                {
                    id = x.Id,
                    Name = x.Name
                }).ToList());
            }
            catch (Exception ex)
            {
                //log
                Log l = new Log();
                //get ip address
                string ip = this.httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();
                l.ipAddress = ip;
                l.Date = System.DateTime.Now;
                l.Description = "ServicesController LoadModel için hatalı işlem";
                l.Exception = ex.Message;
                _RepoL.Add(l);
                return Json(false);
            }
        }

        [HttpGet]
        [Route("getColor")]
        public IActionResult getColor()
        {
            try
            {
                //log
                Log l = new Log();
                //get ip address
                string ip = this.httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();
                l.ipAddress = ip;
                l.Date = System.DateTime.Now;
                l.Description = "ServicesController getColor isteği";
                _RepoL.Add(l);
                return Json(_RepoCo.GetAll());
            }
            catch (Exception ex)
            {
                //log
                Log l = new Log();
                //get ip address
                string ip = this.httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();
                l.ipAddress = ip;
                l.Date = System.DateTime.Now;
                l.Description = "ServicesController getColor için hatalı işlem";
                l.Exception = ex.Message;
                _RepoL.Add(l);
                return Json(false);
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MyApi.Model
{
  public class ProductImageModel
  {
    [Required]
    public string Image1 { get; set; }
    public string Image2 { get; set; }
    public string Image3 { get; set; }
  }
}

﻿using MyApi.Model;
using MyApi.Model.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace MyApi.Repository
{
  public class UnitOfWork : IUnitOfWork
  {

    private readonly Context _context;

    public UnitOfWork(Context dbContext)
    {
      _context=dbContext;
    }

    public void Dispose()
    {
      if (_context != null)
      {
        _context.Dispose();
      }
    }

    public async Task<int> Commit()
    {
      return await _context.SaveChangesAsync();
    }

    public IRepository<T> GetRepository<T>() where T : class
    {
      return new Repository<T>(_context);
    }
  }
}

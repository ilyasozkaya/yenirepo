﻿using MyApi.Model;
using MyApi.Model.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyApi.Repository
{
  public interface IUnitOfWork : IDisposable
  {
    Task<int> Commit();
    IRepository<T> GetRepository<T>() where T : class;
  }
}
